package com.everis.apirest.service;

import com.everis.apirest.model.entity.Producto;

public interface ProductoService {
	public Producto crearProducto (Producto request);
	public Producto obtenerIdProducto(Long id)  throws Exception;
	public Iterable<Producto> obtenerProducto();
}
