package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.ProductoReducidoResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.service.ProductoService;

@RestController
public class ApiController {
	@Autowired
	ProductoService productoService;
	
	@GetMapping("producto/{id}")
	public ProductoResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception  {
		Producto producto = productoService.obtenerIdProducto(id);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setIdProducto(producto.getIdProducto());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());
		return productoResource;
	}
	
	@PostMapping("/producto")
	public ProductoResource crearProducto(@RequestBody ProductoReducidoResource request) throws Exception {
		ProductoResource productoResource = new ProductoResource();
		Producto productoInto = new Producto();
		productoInto.setNombre(request.getNombre());
		productoInto.setDescripcion(request.getDescripcion());
		productoInto.setUnidadMedida(request.getUnidadMedida());
		Producto productoOut = productoService.crearProducto(productoInto);
		productoResource.setIdProducto(productoOut.getIdProducto());
		productoResource.setDescripcion(productoOut.getDescripcion());
		productoResource.setNombre(productoOut.getNombre());
		productoResource.setUnidadMedida(productoOut.getUnidadMedida());
		return productoResource;
	}
	
	@GetMapping("/productos")
	public List<ProductoResource> obtenerProductos (){
		List<ProductoResource> lista = new ArrayList<>();
		productoService.obtenerProducto().forEach(producto->{
			ProductoResource productoResource = new ProductoResource();
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setIdProducto(producto.getIdProducto());
			productoResource.setNombre(producto.getNombre());
			productoResource.setUnidadMedida(producto.getUnidadMedida());
			lista.add(productoResource);
		});
		return lista;
	}
	
}
