package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class ProductoReducidoResource {
	private String descripcion;
	private String nombre;
	private String unidadMedida;
}
